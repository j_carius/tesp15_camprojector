#include "opencv2/opencv.hpp"
#include "aruco.h"

int
main()
{
    // source image to be warped
    cv::Mat lena = cv::imread("../lena.jpg");
    std::vector<cv::Point2f> src_pt(4), dst_pt(4);
    src_pt[0] = cv::Point2f(.0f, .0f);
    src_pt[1] = cv::Point2f(.0f, 255.0f);
    src_pt[2] = cv::Point2f(255.0f, 255.0f);
    src_pt[3] = cv::Point2f(255.0f, .0f);

    // map from marker_id to vertex order
    std::map<int, int> m2v;
    m2v[100] = 0;
    m2v[101] = 1;
    m2v[102] = 2;
    m2v[103] = 3;

    // init camera and images
    cv::VideoCapture cap(0);
    cv::Mat img;
    cv::namedWindow("disp");
    cap >> img;
    int width = img.cols;
    int height = img.rows;

    // init marker detector
    aruco::MarkerDetector mdetector;
    std::vector<aruco::Marker> marker;
    aruco::CameraParameters camera_params; // unknown params
    float marker_size = -1; // unknown size
    double thresh1, thresh2;
    mdetector.getThresholdParams(thresh1, thresh2);
    mdetector.setCornerRefinementMethod(aruco::MarkerDetector::SUBPIX);


    while (1) {
        // get an image
        cap >> img;

        // detect markers
        mdetector.detect(img, marker, camera_params, marker_size);

        // number of detected markers
        int num = (int)marker.size();

        for (int i = 0; i < num; i++){ // i-th detected marker

            // print marker info
            int marker_id = marker[i].id;
            float xpos = marker[i].getCenter().x;
            float ypos = marker[i].getCenter().y;
            std::vector<cv::Point2f> pt(4);
            for (int v = 0; v < 4; v++) {
                pt[v] = marker[i][v];
            }
            printf("id %d centered at [%f, %f]\n", marker_id, xpos, ypos);
            printf("corners: [%f, %f], [%f, %f], [%f, %f], [%f, %f]\n\n",
                   pt[0].x, pt[0].y,
                   pt[1].x, pt[1].y,
                   pt[2].x, pt[2].y,
                   pt[3].x, pt[3].y);

            // draw marker info
            cv::putText(img, 
                        cv::format("%d (%.2f, %.2f)", marker_id, xpos, ypos),
                        cv::Point((int)xpos + 15, (int)ypos),
                        CV_FONT_HERSHEY_PLAIN, 
                        1.0, CV_RGB(255, 0, 0));
            cv::line(img,
                     cv::Point((int)xpos - 10, (int)ypos),
                     cv::Point((int)xpos + 10, (int)ypos),
                     CV_RGB(255, 0, 0), 3);
            cv::line(img,
                     cv::Point((int)xpos, (int)ypos - 10),
                     cv::Point((int)xpos, (int)ypos + 10),
                     CV_RGB(255, 0, 0), 3);

            if (m2v.find(marker_id) != m2v.end()) {
                dst_pt[m2v[marker_id]] = cv::Point2f(xpos, ypos);
            }
        }

        if (num >= 4) {
            // calculate homography matrix
            cv::Mat H = findHomography(src_pt, dst_pt);

            // warp lena onto img
            cv::warpPerspective(lena, img, H, img.size(),
                                cv::INTER_LINEAR,
                                cv::BORDER_TRANSPARENT);
        }

        cv::imshow("disp", img);
		
        if (cv::waitKey(30) > 0) {
            break;
        }
    }

    return 0;
}
