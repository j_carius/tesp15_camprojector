#define NOMINMAX
#include <windows.h> 

#include "opencv2/opencv.hpp"


/* ==================================================================/
 *  Utility functions
 * ================================================================== */
typedef struct {
    int dispno;
    int arraysz;
    MONITORINFO *minfo;
} MonList;

bool CALLBACK
monCallback(HMONITOR hMonitor, HDC hdc, LPRECT lprcMonitor, LPARAM dwData)
{
    MonList *ml = (MonList *)dwData;
    ml->dispno++;

    if (ml->dispno > ml->arraysz) {
        ml->arraysz *= 2;
        ml->minfo = (MONITORINFO *)
            realloc(ml->minfo, sizeof(MONITORINFO) * ml->arraysz);
    }
    ml->minfo[ml->dispno - 1].cbSize = sizeof(MONITORINFO);
    GetMonitorInfo(hMonitor, &(ml->minfo[ml->dispno - 1]));

    return TRUE;
}

MonList *
createMonList(HDC hdc, LPCRECT lprcClip)
{
    MonList *ml = (MonList *)malloc(sizeof(MonList));
    ml->dispno = 0;
    ml->arraysz = 1;
    ml->minfo = (MONITORINFO *)malloc(sizeof(MONITORINFO));
    EnumDisplayMonitors(hdc, lprcClip,
                        (MONITORENUMPROC)monCallback, (LPARAM)ml);
    return ml;
}

int
releaseMonList(MonList **ml)
{
    free((*ml)->minfo);
    free(*ml);
    return 0;
}

typedef struct {
    int width;
    int height;
    int x0; // x position of top-left corner
    int y0; // y position of top-left corner
    int monitor_id;
} ScreenInfo;

int
getScreenInfo(int monitor_id, ScreenInfo *si)
{
    if (monitor_id < 0) {
        // Assume a single monitor; monitor enumeration is skipped
        si->width = GetSystemMetrics(SM_CXSCREEN);
        si->height = GetSystemMetrics(SM_CYSCREEN);
        si->x0 = 0;
        si->y0 = 0;
        si->monitor_id = 0;
        return 0;
    }

    MonList *ml = createMonList(NULL, NULL);
    if (monitor_id >= ml->dispno) {
        fprintf(stderr, "invalid monitor_id %d (# Displays: %d)\n",
                monitor_id, ml->dispno);
        return -1;
    }
    MONITORINFO *mp = &(ml->minfo[monitor_id]);
    si->width = mp->rcMonitor.right - mp->rcMonitor.left;
    si->height = mp->rcMonitor.bottom - mp->rcMonitor.top;
    si->x0 = mp->rcMonitor.left;
    si->y0 = mp->rcMonitor.top;
    si->monitor_id = monitor_id;
    
    releaseMonList(&ml);
    return 0;
}

int
undecorateWindow(const char *win)
{
    HWND hwnd_child = (HWND)cvGetWindowHandle(win);
    HWND hwnd = GetParent(hwnd_child);

    LONG sty = GetWindowLong(hwnd, GWL_STYLE);
    sty &= ~WS_CAPTION; // no title bar
    SetWindowLong(hwnd, GWL_STYLE, sty); 

    sty = GetWindowLong(hwnd_child, GWL_STYLE);
    sty &= ~WS_THICKFRAME; // no thick frame
    sty &= ~WS_BORDER; // no border
    SetWindowLong(hwnd_child, GWL_STYLE, sty);

    return 0;
}

int
setWindowFullscreen(const char *win, ScreenInfo *si)
{
    HWND hwnd_child = (HWND)cvGetWindowHandle(win);
    HWND hwnd = GetParent(hwnd_child);

    SetWindowPos(hwnd, HWND_TOPMOST,
                 si->x0, si->y0, si->width, si->height, SWP_SHOWWINDOW);
    return 0;
}

int
setWindowTranslucent(const char *win, unsigned char alpha)
{
    HWND hwnd_child = (HWND)cvGetWindowHandle(win);
    HWND hwnd = GetParent(hwnd_child);

    LONG sty = GetWindowLong(hwnd, GWL_EXSTYLE);
    sty |= WS_EX_LAYERED;
    SetWindowLong(hwnd, GWL_EXSTYLE, sty);

    SetLayeredWindowAttributes(hwnd, 0, alpha, LWA_ALPHA);

    return 0;
}

int
setWindowChromaKeyed(const char *win, CvScalar color)
{
    HWND hwnd_child = (HWND)cvGetWindowHandle(win);
    HWND hwnd = GetParent(hwnd_child);

    LONG sty = GetWindowLong(hwnd, GWL_EXSTYLE);
    sty |= WS_EX_LAYERED;
    SetWindowLong(hwnd, GWL_EXSTYLE, sty);

    COLORREF crkey = RGB(color.val[2], color.val[1], color.val[0]);
    SetLayeredWindowAttributes(hwnd, crkey, 0, LWA_COLORKEY);

    return 0;
}

/* ================================================================= */



int
main()
{
    cv::Mat image = cv::imread("../lena.jpg");
    if (image.empty()) {
        fprintf(stderr, "file cannot be loaded\n");
        exit(1);
    }

    cv::namedWindow("disp");

    /* Remove title bars and frames */
    undecorateWindow("disp"); 

    /* Make the window translucent */ 
    //setWindowTranslucent("disp", 200); // 0: transparent, 255: opaque

    /* Make the window chroma-keyed */ 
    //setWindowChromaKeyed("disp", CV_RGB(0, 0, 0));

    /* Obtain geometrical information of a specified screen */
    ScreenInfo si;
    getScreenInfo(0, &si); // (monitor_id, pointer to ScreenInfo)

    /* Bring the window topmost at the origin of the specified screen */
    setWindowFullscreen("disp", &si);


    /* Resize the image to fit the fullscreen size */
    cv::Mat image_fit;
    cv::resize(image, image_fit, cv::Size(si.width, si.height),
               0, 0, cv::INTER_LINEAR);

    //cv::imshow("disp", image); // original image
    cv::imshow("disp", image_fit); // fullscreen-size image
    cv::waitKey(0);
    return 0;
}
