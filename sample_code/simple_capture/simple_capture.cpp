#include "opencv2/opencv.hpp"

int main()
{
    cv::VideoCapture cap(0); // first USB camera
    cv::Mat image;
    cv::namedWindow("disp", CV_WINDOW_AUTOSIZE);

    while (1) {
        // obtain an image from camera
        cap >> image;

        cv::imshow("disp", image);
        if (cv::waitKey(30) > 0) { // wait for key hit for 30 ms
            break;
        }
    }
    return 0;
}
