#include "opencv2/opencv.hpp"
#include "ARToolKitPlus/TrackerMultiMarkerImpl.h"

int
main()
{
    // source image to be warped
    cv::Mat lena = cv::imread("../lena.jpg");
    std::vector<cv::Point2f> src_pt(4), dst_pt(4);
    src_pt[0] = cv::Point2f(.0f, .0f);
    src_pt[1] = cv::Point2f(.0f, 255.0f);
    src_pt[2] = cv::Point2f(255.0f, 255.0f);
    src_pt[3] = cv::Point2f(255.0f, .0f);

    // map from marker_id to vertex order
    std::map<int, int> m2v;
    m2v[0] = 0;
    m2v[1] = 1;
    m2v[2] = 2;
    m2v[3] = 3;

    // init camera and images
    cv::VideoCapture cap(0);
    cv::Mat img;
    cv::namedWindow("disp");
    cap >> img;
    int width = img.cols;
    int height = img.rows;

    // init marker tracker
    const float near_len = 1.0;
    const float far_len = 10000.0;
    ARToolKitPlus::TrackerMultiMarker *tracker;
    tracker = new
        ARToolKitPlus::TrackerMultiMarkerImpl<6,6,6,32,8>(width, height);
                               // args to TrackerMultiMarkerImpl:
                               //   <marker_width,
                               //    marker_height,
                               //    sampling step,
                               //    max num in cfg, max num in scene>
    tracker->init("camera_param.cal", "marker_config.cfg", near_len, far_len);
    tracker->setPixelFormat(ARToolKitPlus::PIXEL_FORMAT_BGR);
    tracker->setBorderWidth(0.125f); // thin frame marker
    tracker->activateAutoThreshold(true);
    tracker->setUndistortionMode(ARToolKitPlus::UNDIST_NONE);
    tracker->setPoseEstimator(ARToolKitPlus::POSE_ESTIMATOR_RPP);
    tracker->setMarkerMode(ARToolKitPlus::MARKER_ID_BCH);

    while (1) {
        // get an image
        cap >> img;

        // detect markers
        tracker->calc((unsigned char *)(img.data));

        // number of detected markers
        int num = tracker->getNumDetectedMarkers();

        for (int i = 0; i < num; i++){ // i-th detected marker

            // print marker info
            ARToolKitPlus::ARMarkerInfo a = tracker->getDetectedMarker(i);
            int marker_id = a.id;
            float xpos = a.pos[0];
            float ypos = a.pos[1];
            std::vector<cv::Point2f> pt(4);
            for (int v = 0; v < 4; v++) {
                pt[v] = cv::Point2f(a.vertex[v][0], a.vertex[v][1]);
            }
            printf("id %d centered at [%f, %f]\n", marker_id, xpos, ypos);
            printf("corners: [%f, %f], [%f, %f], [%f, %f], [%f, %f]\n\n",
                   pt[0].x, pt[0].y,
                   pt[1].x, pt[1].y,
                   pt[2].x, pt[2].y,
                   pt[3].x, pt[3].y);

            // draw marker info
            cv::putText(img, 
                        cv::format("%d (%.2f, %.2f)", marker_id, xpos, ypos),
                        cv::Point((int)xpos + 15, (int)ypos),
                        CV_FONT_HERSHEY_PLAIN, 
                        1.0, CV_RGB(255, 0, 0));
            cv::line(img,
                     cv::Point((int)xpos - 10, (int)ypos),
                     cv::Point((int)xpos + 10, (int)ypos),
                     CV_RGB(255, 0, 0), 3);
            cv::line(img,
                     cv::Point((int)xpos, (int)ypos - 10),
                     cv::Point((int)xpos, (int)ypos + 10),
                     CV_RGB(255, 0, 0), 3);

            if (m2v.find(marker_id) != m2v.end()) {
                dst_pt[m2v[marker_id]] = cv::Point2f(xpos, ypos);
            }
        }

        if (num >= 4) {
            // calculate homography matrix
            cv::Mat H = findHomography(src_pt, dst_pt);

            // warp lena onto img
            cv::warpPerspective(lena, img, H, img.size(),
                                cv::INTER_LINEAR,
                                cv::BORDER_TRANSPARENT);
        }

        cv::imshow("disp", img);
		
        if (cv::waitKey(30) > 0) {
            break;
        }
    }

    return 0;
}
