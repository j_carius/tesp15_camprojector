#include "opencv2/opencv.hpp"
#include "aruco.h"
#include "arucofidmarkers.h"

int main(int argc, char *argv[])
{
    if (argc != 4){
        std::cerr << "Usage: " << argv[0] << " marker_id outfile size"
                  << std::endl;
        std::cerr << " marker_id: integer from 0 to 1023" << std::endl;
        std::cerr << " outfile: file name such as otuput.png" << std::endl;
        std::cerr << " size: marker size on a side [pixel]" << std::endl;
        return -1;
    }
    int marker_id = atoi(argv[1]);
    if (marker_id < 0 || 1023 < marker_id) {
        std::cerr << " marker_id: integer from 0 to 1023" << std::endl;
        return -1;
    }

    cv::Mat marker =
        aruco::FiducidalMarkers::createMarkerImage(marker_id, atoi(argv[3]));
    cv::imwrite(argv[2], marker);

    return 0;
}
