/*
 * brisk_detector.h
 *
 *  Created on: Jul 30, 2015
 *      Author: jan
 */

#ifndef SRC_BRISK_DETECTOR_H_
#define SRC_BRISK_DETECTOR_H_

#include "opencv2/opencv.hpp"

std::vector<cv::DMatch> brisk_findmatches(cv::Mat screen_img, cv::VideoCapture cap, cv::Ptr<cv::BRISK> briskd, cv::BFMatcher matcher);



#endif /* SRC_BRISK_DETECTOR_H_ */
