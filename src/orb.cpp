#include <iostream>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <cstdlib>

using namespace cv;
using namespace std;
int main ()
{
cv::namedWindow("disp", CV_WINDOW_AUTOSIZE);
    cv::VideoCapture cap(1);
    cv::Mat color_image, image, src;

    // keypoint detector and matcher
    cv::ORB orb(500, 1.2f, 8, 4, 0, 2, cv::ORB::HARRIS_SCORE);
    cv::BFMatcher hamming_matcher(cv::NORM_HAMMING, false);

    // First, take a source image
    printf("Hit any key to capture a source image\n");
    while (1) {
        cap >> color_image;
        cv::imshow("disp", color_image);
        if (cv::waitKey(30) > 0) {
            cv::cvtColor(color_image, src, CV_BGR2GRAY);
            break;
        }
    }
	
    // define corner points so that we can draw a transformed rectangle
    std::vector<cv::Point2f> corner(5);
    corner[0] = cv::Point2f(0.0f, 0.0f);
    corner[1] = cv::Point2f(0.0f, (float)src.rows - 1);
    corner[2] = cv::Point2f((float)src.cols - 1,
                            (float)src.rows - 1);
    corner[3] = cv::Point2f((float)src.cols - 1, 0.0f);
    corner[4] = cv::Point2f((float)src.cols/2,(float)src.rows/2);	
    // detect keypoints and extract descriptors from source image
    std::vector<cv::KeyPoint> keypoint_src;
    cv::Mat descriptor_src;
    orb(src, cv::noArray(), keypoint_src, descriptor_src);
	std::cout << "hello" << std::endl;
    

    while (1) {
        cap >> color_image;
        cv::cvtColor(color_image, image, CV_BGR2GRAY);
        
        
        // detect keypoints and extract descriptors from destination image
        cv::Mat descriptor_dst;
        std::vector<cv::KeyPoint> keypoint_dst;
        orb(image, cv::noArray(), keypoint_dst, descriptor_dst);

        // match the keypoints; two most resembling keypoints are
        // picked up, and if the better one is significantly better
        // than the other, it is treated as a 'good' match.
        std::vector<std::vector<cv::DMatch> > matches;
        hamming_matcher.knnMatch(descriptor_src, descriptor_dst, matches, 2);
        std::vector<cv::DMatch> good_matches;
        for (int i = 0; i < (int)matches.size(); i++) {
            if (matches[i].size() == 1 ||
                matches[i][0].distance < 0.7 * matches[i][1].distance) {
                good_matches.push_back(matches[i][0]);
            }
        }

        // draw the matched points
        cv::Mat img_matches;
        cv::drawMatches(src, keypoint_src, image, keypoint_dst,
                        good_matches, img_matches,
                        cv::Scalar::all(-1), cv::Scalar::all(-1),
                        std::vector<char>(), 
                        cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
            
        if (good_matches.size() >= 10) {
            // define points pairs for homography computation
            std::vector<cv::Point2f> goodpt_src, goodpt_dst;
            for (int i = 0; i < (int)good_matches.size(); i++) {
                goodpt_src.push_back(keypoint_src[good_matches[i].queryIdx].pt);
                goodpt_dst.push_back(keypoint_dst[good_matches[i].trainIdx].pt);
            }

            // Use RANSAC algorithm to make use of more than four pairs
            cv::Mat mask;
            cv::Mat H = cv::findHomography(goodpt_src, goodpt_dst,
                                           CV_RANSAC, 3, mask);

            // draw transformed rectangle
            std::vector<cv::Point2f> corner_dst;
            cv::perspectiveTransform(corner, corner_dst, H);
            std::vector<cv::Point> corner_dst_matches(5);
            for (int i = 0; i < 4; i++) {
                corner_dst_matches[i].x = (int)corner_dst[i].x + src.cols;
                corner_dst_matches[i].y = (int)corner_dst[i].y;
            }
	    corner_dst_matches[4].x = (int)corner_dst[4].x + src.cols;
            corner_dst_matches[4].y = (int)corner_dst[4].y;
            cv::polylines(img_matches, corner_dst_matches, 1,
                          CV_RGB(255, 0, 0), 3);
            circle( img_matches, cv::Point( (int)corner_dst_matches[4].x, (int)corner_dst_matches[4].y ), 32.0, Scalar( 0, 0, 255 ), -1, 8 );
            int camx,camy;
            if(((int)corner_dst_matches[4].x)>1280)
            {     camx = 640;}
            else if(((int)corner_dst_matches[4].x)<640)
            {     camx = 0;}
            else
            { camx = std::abs(((int)corner_dst_matches[4].x)-640); }
            if ( (int)corner_dst_matches[4].y > 480 )
            { camy = 480;}
            else if( (int)corner_dst_matches[4].y < 0 )
            { camy = 0; }
            else
            { camy = ((int)corner_dst_matches[4].y); }
            std::cout << "x_cam-center" << camx<< std::endl;
            //std::cout << "hello" << img_matches.rows << img_matches.cols << std::endl;
            /*if(((int)corner_dst_matches[4].x)>=(img_matches.cols/4)*3)
            	{std::cout << "x_greater" << std::endl;}
            else
                {std::cout << "x_lower" << std::endl;}*/
            // highlight the points used to compute homography
            for (int i = 0; i < mask.rows; i++) {
                if (mask.at<uchar>(i) == 0) {
                    continue;
                }
                cv::circle(img_matches,
                           cv::Point((int)goodpt_src[i].x,
                                     (int)goodpt_src[i].y),
                           3, CV_RGB(255, 0, 0), -1);
            }

        cv::Mat cropedImage1 = img_matches(Rect(0,0,640,480)); 
        circle( cropedImage1,cv::Point( (640-camx) , (480-camy) ), 32.0, Scalar( 0, 0, 255 ), -1, 8 );
        cv::imshow("disp1", cropedImage1);
        }
        
        
        
        //circle( cropedImage1, cv::Point( (int)corner_dst_matches[4].x, (int)corner_dst_matches[4].y ), 32.0, Scalar( 0, 0, 255 ), -1, 8 );
        cv::Mat cropedImage2 = img_matches(Rect(640,0,640,480));
        //cv::imshow("disp", img_matches);
        
        cv::imshow("disp2", cropedImage2);
        if (cv::waitKey(30) > 0) {
            break;
        }
    }



     return 0;
}
