#include "opencv2/opencv.hpp"

int
annotateRegion()
{
    cv::namedWindow("disp", CV_WINDOW_AUTOSIZE);
    const int width = 800;
    const int height = 600;
    cv::Mat img1_orig = cv::imread("map_view.png");
    cv::Mat img2_orig = cv::imread("hybrid_view.png");
    cv::Mat img1 = img1_orig(cv::Rect(400, 50, width, height));
    cv::Mat img2 = img2_orig(cv::Rect(400, 50, width, height));
    cv::Mat img3 = img1.clone();
    // const int width = 640;
    // const int height = 480;
    // cv::Mat img1_orig = cv::imread("sendai_map.jpg");
    // cv::Mat img2_orig = cv::imread("sendai_map_photo.jpg");
    // cv::Mat img1 = img1_orig(cv::Rect(300, 0, width, height));
    // cv::Mat img2 = img2_orig(cv::Rect(300, 0, width, height));


    int off = -200.0;
    while (off < 200.0) {
        img3 = img1.clone();

        cv::Mat warp_src(256, 256, CV_8U);
        cv::Mat mask(height, width, CV_8U);
        warp_src = 255; // white
        mask = 0; // black
        cv::Mat H = (cv::Mat_<double>(3, 3) <<
                     1.0, 0.0, width / 2.0 - off,
                     0.0, 1.0, height / 2.0 - off,
                     0.0, 0.0, 1);
        // mask image
        cv::warpPerspective(warp_src, mask, H, mask.size(),
                            cv::INTER_NEAREST, cv::BORDER_TRANSPARENT);
        //cv::imshow("disp", mask);
        // cv::waitKey(0);
        // // original image
        // cv::imshow("disp", img1);
        // cv::waitKey(0);
        // modified image

        img2.copyTo(img3, mask);

        cv::imshow("disp", img3);
        cv::waitKey(0);
        off += 10;

    }

    return 0;
}
