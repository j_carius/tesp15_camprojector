/*
 * brisk_detector.cpp
 *
 *  Created on: Jul 29, 2015
 *      Author: jan
 *
 *  This is the BRISK detector algorithm.
 *  Arguments are two pictures given as BW images in cv::Mat format
 *
 *  Implementation according to http://stackoverflow.com/questions/13423884/how-to-use-brisk-in-opencv
 *  NOTE: Adapted to openCV 3 syntax
 */

#include "opencv2/opencv.hpp"
#include <sstream>
#include <string>

std::vector<cv::DMatch> brisk_findmatches(cv::Mat screen_img,
		cv::VideoCapture cap, cv::Ptr<cv::BRISK> briskd,
		cv::BFMatcher matcher) {

	bool show_homography = true;
	bool draw_matches = false;

	const unsigned int SCREEN_RES_X = 1000;
	const unsigned int SCREEN_RES_Y = 558;
	const unsigned int CAM_RES_X = 640;
	const unsigned int CAM_RES_Y = 480;

	//define keypoint and descriptor variables
	std::vector<cv::KeyPoint> keypointsCam, keypointsScreen;
	cv::Mat descriptorsCam, descriptorsScreen;

	//get image from camera
	cv::Mat cam_image, cam_image_bw;
	cap >> cam_image;
	cv::cvtColor(cam_image, cam_image_bw, CV_BGR2GRAY);

	//detect descriptors and compute keypoints in both images
	briskd->detect(screen_img, keypointsScreen);
	briskd->detect(cam_image_bw, keypointsCam);
	std::cout << "detected keypoints" << std::endl;
	briskd->compute(screen_img, keypointsScreen, descriptorsScreen);
	briskd->compute(cam_image_bw, keypointsCam, descriptorsCam);
	std::cout << "calculated descriptors" << std::endl;

	//carry out the matching
	std::vector < cv::DMatch > matches;
	std::vector < std::vector<cv::DMatch> > matches_knn;
	matcher.match(descriptorsScreen, descriptorsCam, matches);
	matcher.knnMatch(descriptorsScreen, descriptorsCam, matches_knn, 2);
	std::cout << "matched points" << std::endl;

	if (show_homography) {

		std::vector < cv::Point2f > corner(5);
		corner[0] = cv::Point2f(0.0f, 0.0f);
		corner[1] = cv::Point2f(0.0f, (float) cam_image_bw.rows - 1);
		corner[2] = cv::Point2f((float) cam_image_bw.cols - 1,
				(float) cam_image_bw.rows - 1);
		corner[3] = cv::Point2f((float) cam_image_bw.cols - 1, 0.0f);
		corner[4] = cv::Point2f((float) cam_image_bw.cols / 2,
				(float) cam_image_bw.rows / 2);

		std::vector < cv::DMatch > good_matches;
		for (int i = 0; i < (int) matches.size(); i++) {
			if (matches_knn[i].size() == 1
					|| matches_knn[i][0].distance
							< 0.7 * matches_knn[i][1].distance) {
				good_matches.push_back(matches_knn[i][0]);
			}
		}

		// draw the matched points
		cv::Mat img_matches;

		if (draw_matches) {
			cv::drawMatches(screen_img, keypointsScreen, cam_image_bw,
					keypointsCam, good_matches, img_matches,
					cv::Scalar::all(-1), cv::Scalar::all(-1),
					std::vector<char>(),
					cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		} else {
			cv::hconcat(screen_img, cam_image_bw, img_matches);
		}
		cv::cvtColor(img_matches, img_matches, CV_GRAY2RGB);

		std::cout << "Number of good matches = " << good_matches.size()
				<< std::endl;

		if (good_matches.size() >= 10) {
			// define points pairs for homography computation
			std::vector<cv::Point2f> goodpt_screen_img, goodpt_dst;
			for (int i = 0; i < (int) good_matches.size(); i++) {
				goodpt_screen_img.push_back(
						keypointsScreen[good_matches[i].queryIdx].pt);
				goodpt_dst.push_back(keypointsCam[good_matches[i].trainIdx].pt);
			}

			// Use RANSAC algorithm to make use of more than four pairs
			cv::Mat mask;
			cv::Mat H = cv::findHomography(goodpt_screen_img, goodpt_dst,
					CV_RANSAC, 3, mask);

			// draw transformed rectangle
			std::vector < cv::Point2f > corner_dst;
			try {
				cv::perspectiveTransform(corner, corner_dst, H);
			} catch (...) {
				corner_dst = corner;
			}

			std::vector < cv::Point > corner_dst_matches(5);
			for (int i = 0; i < 4; i++) {
				corner_dst_matches[i].x = (int) corner_dst[i].x
						+ screen_img.cols;
				corner_dst_matches[i].y = (int) corner_dst[i].y;
			}
			corner_dst_matches[4].x = (int) corner_dst[4].x + screen_img.cols;
			corner_dst_matches[4].y = (int) corner_dst[4].y;
			cv::polylines(img_matches, corner_dst_matches, 1, CV_RGB(255, 0, 0),
					3);
			circle(img_matches,
					cv::Point((int) corner_dst_matches[4].x,
							(int) corner_dst_matches[4].y), 32.0,
					cv::Scalar(0, 0, 255), -1, 8);
			int camx, camy;
			if (((int) corner_dst_matches[4].x) > 2 * CAM_RES_X) {
				camx = CAM_RES_X;
			} else if (((int) corner_dst_matches[4].x) < CAM_RES_X) {
				camx = 0;
			} else {
				camx = std::abs(((int) corner_dst_matches[4].x) - CAM_RES_X);
			}
			if ((int) corner_dst_matches[4].y > CAM_RES_Y) {
				camy = CAM_RES_Y;
			} else if ((int) corner_dst_matches[4].y < 0) {
				camy = 0;
			} else {
				camy = ((int) corner_dst_matches[4].y);
			}
			std::cout << "x_cam-center" << camx << std::endl;
			//std::cout << "hello" << img_matches.rows << img_matches.cols << std::endl;
			/*if(((int)corner_dst_matches[4].x)>=(img_matches.cols/4)*3)
			 {std::cout << "x_greater" << std::endl;}
			 else
			 {std::cout << "x_lower" << std::endl;}*/
			// highlight the points used to compute homography
			for (int i = 0; i < mask.rows; i++) {
				if (mask.at < uchar > (i) == 0) {
					continue;
				}
				cv::circle(img_matches,
						cv::Point((int) goodpt_screen_img[i].x,
								(int) goodpt_screen_img[i].y), 3,
						CV_RGB(255, 0, 0), -1);
			}

			cv::Mat cropedImage1 = img_matches(
					cv::Rect(0, 0, CAM_RES_X, CAM_RES_Y));
			circle(cropedImage1,
					cv::Point((CAM_RES_X - camx), (CAM_RES_Y - camy)), 32.0,
					cv::Scalar(0, 0, 255), -1, 8);
			cv::imshow("homographyView1", cropedImage1);

			std::cout << "99" << std::endl;
			//circle( cropedImage1, cv::Point( (int)corner_dst_matches[4].x, (int)corner_dst_matches[4].y ), 32.0, Scalar( 0, 0, 255 ), -1, 8 );
			cv::Mat cropedImage2 = img_matches(
					cv::Rect(CAM_RES_X, 0, CAM_RES_X, CAM_RES_Y));
			//cv::imshow("disp", img_matches);

			cv::imshow("homographyView2", cropedImage2);
			cv::waitKey(10);
		}

	}

	return matches;
}
