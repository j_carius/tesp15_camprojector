/*
 * keypoints_homography.h
 *
 *  Created on: Jul 28, 2015
 *      Author: jan
 */

#ifndef SRC_KEYPOINTS_HOMOGRAPHY_H_
#define SRC_KEYPOINTS_HOMOGRAPHY_H_

#include "opencv2/opencv.hpp"

int keypoint_homography();



#endif /* SRC_KEYPOINTS_HOMOGRAPHY_H_ */
