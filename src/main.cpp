/*
 * main.cpp
 *
 *  Created on: Jul 28, 2015
 *      Author: jan
 *  This is the main file used to call every aspect of the project
 */

#include "simple_capture.h"
//#include "keypoints_homography.h"
#include "annotate_region.h"
#include "brisk_detector.h"

#include <iostream>

int main() {

	//annotateRegion(); // just testing
	//keypoint_homography();
	//showWebcam();

	const char * PimA = "img2.jpg"; // imgA. Put images in directory of executable
	cv::Mat imgA = cv::imread(PimA, CV_LOAD_IMAGE_GRAYSCALE);

	cv::namedWindow("homographyView1", CV_WINDOW_NORMAL);
	cv::namedWindow("homographyView2", CV_WINDOW_NORMAL);

	cv::VideoCapture cap(1); // first USB camera
	while (!cap.isOpened())  // check if we succeeded opening camera
	{
		std::cout
				<< "Could not capture from camera. Retrying...   Press any key to quit."
				<< std::endl;
		if (cv::waitKey(500) > 0)
			return -1;
	}

	//set brisk parameters
	const int Threshl = 60; //try 60
	const int Octaves = 3; //try 0 //(pyramid layer) from which the keypoint has been extracted
	const float PatternScales = 1.0f;
	cv::Ptr < cv::BRISK > briskd = cv::BRISK::create(Threshl, Octaves,
			PatternScales);

	cv::BFMatcher matcher(cv::NORM_HAMMING, false);
	//cv::FlannBasedMatcher matcher(new cv::flann::LshIndexParams(20,10,2)); //TODO: adjust parameters or matcher if necessary

	std::vector < cv::DMatch > matches;
	std::cout << "Entering loop" << std::endl;

	while (true) // run loop of the program
	{
		// obtain an image from camera
		matches = brisk_findmatches(imgA, cap, briskd, matcher);

		//if (cv::waitKey(30) > 0) // wait for key hit for 30 ms
		//	break;
	}

	return 0;
}

