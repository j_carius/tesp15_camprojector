#!/usr/bin/python

import numpy as np
import cv2
import math


class CamPointer:
    def __init__(self, image_path, camera=-1):
        self.MIN_MATCH_COUNT = 10
        self.MAX_MATCH_COUNT = 20

        cv2_version = int((cv2.__version__).split('.')[0])
        if cv2_version <= 2:
            self.orb = cv2.ORB()
        else:
            self.orb = cv2.ORB_create()

        self.bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        self.cap = cv2.VideoCapture(camera)

        self.img1 = cv2.imread(image_path, 0)           # BW version, for feature matching
        self.img1_height, self.img1_width = self.img1.shape[:2]
        self.kp1, self.des1 = self.orb.detectAndCompute(self.img1, None)
        self.pos = None

    def get_pos(self):
        return self.pos

    def update_pos(self):
        ret, img2 = self.cap.read()
        if not ret:
            print "No image from camera"
            return

        img2_height, img2_width = img2.shape[:2]
        refpoints = [[img2_width/2, img2_height/2], [img2_width/2, 0]]
        kp2, des2 = self.orb.detectAndCompute(img2, None)

        if des2 is None:
            print "No matches found."
            return

        matches = self.bf.match(self.des1,des2)
        matches = sorted(matches, key = lambda x:x.distance)
        good = matches[:self.MAX_MATCH_COUNT]

        if len(good) < self.MIN_MATCH_COUNT:
            print "Not enough matches are found - %d/%d" % (len(good), self.MIN_MATCH_COUNT)
            return

        src_pts = np.float32([ self.kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC,5.0)
        
        src = np.float32(refpoints).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(src,M)

        middle = (dst[0][0][0], dst[0][0][1])
        top = (dst[1][0][0], dst[1][0][1])
        dx = top[0]-middle[0]
        dy = top[1]-middle[1]

        angle = math.atan2(dy, dx)
        angle = (270 - math.degrees(angle)) % 360   # Convert to degrees, anticlockwise
        self.pos = middle, angle
        return
            
    def __del__(self):
        self.cap.release()

if __name__ == '__main__':
    img_path = 'img2.jpg'
    cv2.namedWindow('img', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('img', 966, 768)
    cv2.moveWindow('img', 400, 0)
    display = cv2.imread(img_path, -1)         # Colour version, for display


    campointer = CamPointer(img_path)
    while (True):
        campointer.update_pos()
        print campointer.get_pos()
        cv2.imshow('img', display)
        keypressed = cv2.waitKey(100) & 0xFF
        if keypressed == ord('q'):
            break
