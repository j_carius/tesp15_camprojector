#!/usr/bin/python

import sys, pygame, math
from campointer import CamPointer


class Vector(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __repr__(self):
        return "x: {} y: {}".format(self.x, self.y)
    def __add__(self, other): #overload + operator
        return Vector(self.x+other.x, self.y+other.y)
    def __mul__(self, other): #overload * operator
        if type(other) in (int, float):
            return Vector(self.x*other, self.y*other)
        else:
            raise TypeError('dont know how to multiply with vector')
    def reflect(self, direction):
        product = self.dot_product(direction)
        self.x -= 2*direction.x*product
        self.y -= 2*direction.y*product
    def dot_product(self, other):
        return self.x*other.x + self.y*other.y
    def get_magnitude(self):
        return math.sqrt(self.x**2 + self.y**2)
    def normalise(self):
        magnitude = self.get_magnitude()
        self.x = self.x/magnitude
        self.y = self.y/magnitude
    def rotate(self, angle):
        theta = -1*math.radians(angle)
        rotated_x = self.x*math.cos(theta) - self.y*math.sin(theta)
        rotated_y = self.y*math.cos(theta) + self.x*math.sin(theta)
        self.x = rotated_x
        self.y = rotated_y

class Ball:
    def __init__(self, image_path, x, y, velocity = Vector(0,1)):
        self.image = pygame.image.load(image_path).convert()
        self.image.set_colorkey((255, 255, 255))
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.center = (self.x, self.y)
        self.velocity = velocity

    def move(self):
        self.x += self.velocity.x
        self.y += self.velocity.y
        self.rect.center = (self.x, self.y)

class Paddle:
    def __init__(self, image_path, x, y, angle=0):
        self.unrotated_image = pygame.image.load(image_path).convert()
        self.unrotated_image.set_colorkey((255,255,255))
        self.update(x, y, angle)
    def update(self, x, y, angle=None):
        if angle != None:
            self.angle = int(angle)

        self.image = pygame.transform.rotate(self.unrotated_image, self.angle)
        self.rect = self.image.get_rect()
        self.rect.center = (int(x), int(y))

class Background:
    def __init__(self, image_path):
        self.image = pygame.image.load(image_path).convert()
        self.rect = self.image.get_rect()

if __name__ == '__main__':
    pygame.init()
    size = width, height = 1000, 500
    screen = pygame.display.set_mode(size)

    bg_img = 'img2.jpg'
    background = Background(bg_img)

    campointer = CamPointer(bg_img)

    speed = Vector(0, 30)

    ball = Ball("ball.png", width/2, height/4, speed)
    paddle = Paddle("paddle.png", width/2, height*3/4, 0)

    clock = pygame.time.Clock()

    while 1:
        clock.tick(40)
        print "FPS:", clock.get_fps()
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        ret = campointer.get_pos()
        if ret:
            (x, y), angle = ret
            paddle.update(x, y, angle)

        if ball.rect.colliderect(paddle.rect):
            normalvector = Vector(0, -1)
            normalvector.rotate(paddle.angle)
            normalvector.normalise()
            ball.velocity.reflect(normalvector)
        if ball.rect.left < 0:
            ball.velocity.x = abs(ball.velocity.x)
        if ball.rect.right > width:
            ball.velocity.x = -abs(ball.velocity.x)
        if ball.rect.top < 0:
            ball.velocity.y = abs(ball.velocity.y)
        if ball.rect.bottom > height:
            ball.velocity.y = -abs(ball.velocity.y)

        ball.move()

        screen.blit(background.image, background.rect)
        screen.blit(paddle.image, paddle.rect)
        screen.blit(ball.image, ball.rect)
        pygame.display.flip()

