#!/usr/bin/python

import sys, pygame, math
from campointer import CamPointer
from ball import *


class Player:
    def __init__(self, x, y, player_img, cam_number, bg_img):
        self.campointer = CamPointer(bg_img, cam_number)
        self.paddle = Paddle(player_img, x, y)

if __name__ == '__main__':
    pygame.init()
    size = width, height = 1000, 500
    screen = pygame.display.set_mode(size)

    bg_img = 'img2.jpg'
    background = Background(bg_img)

    player1 = Player(0, height, "paddle.png", -1, bg_img)
    player2 = Player(width, height, "paddle2.png", -2, bg_img)

    players = [player1, player2]

    speed = Vector(0, 30)
    ball = Ball("ball.png", width/2, height/4, speed)

    dirty_rects = []
    screen.blit(background.image, background.rect)
    pygame.display.flip()

    clock = pygame.time.Clock()

    while 1:
        clock.tick(20)
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        for player in players:
            ret = player.campointer.get_pos()
            if ret:
                (x, y), angle = ret
                player.paddle.update(x, y, angle)

            if ball.rect.colliderect(player.paddle.rect):
                normalvector = Vector(0, -1)
                normalvector.rotate(player.paddle.angle)
                normalvector.normalise()
                ball.velocity.reflect(normalvector)

        if ball.rect.left < 0:
            ball.velocity.x = abs(ball.velocity.x)
        if ball.rect.right > width:
            ball.velocity.x = -abs(ball.velocity.x)
        if ball.rect.top < 0:
            ball.velocity.y = abs(ball.velocity.y)
        if ball.rect.bottom > height:
            ball.velocity.y = -abs(ball.velocity.y)

        ball.move()

        screen.blit(background.image, background.rect)
        pygame.display.update(dirty_rects)
        dirty_rects = []
        for player in players:
            screen.blit(player.paddle.image, player.paddle.rect)
            pygame.display.update(player.paddle.rect)
            dirty_rects.append(player.paddle.rect.copy())

        screen.blit(ball.image, ball.rect)
        pygame.display.update(ball.rect)
        dirty_rects.append(ball.rect.copy())
        print clock.get_fps()

