#!/usr/bin/python

from ball import Vector, Paddle

class catball_bot(object):
	def __init__(self, max_speed, side, width, height):
		#initialize own variables accoding to constructor input
		self.max_speed = max_speed if max_speed > 0 else 1.0 #movement speed, equivalent to difficulty
		if side == 'left' or side == 'right':
			self.right_side = (side == 'right') #which side the bot is playing on, right=true, left=false
		else:
			print 'Wrong initialization of side.'
			raise('Bot init error')
		self.x_target = width-40 if self.right_side else 40

		#state variables of paddle
		self.velocity = Vector(0.0,0.0) #speed in image x,y coordinates (pixel per loop)
		self.position = Vector(width/2, height/2) #position in image x,y coordinates
		self.orientation = 0.0 #angle of orientation [rad]
		self.paddle = Paddle("paddle2.png", x=0, y=0, angle=0)
	
	def update(self, ball_pos, ball_velocity):
		#strategy: place paddle always along line just before the own goal.
		#move up and down according to where ball will come
		#rule: set new velocity and add to current position.

		#calculate desired speeds. always move with max speed unless overshoot
		new_velocity = Vector(0.0,0.0)
		new_velocity.x = self.x_target - self.position.x
		new_velocity.y = ball_pos[1] - self.position.y
		if new_velocity.get_magnitude() > self.max_speed:
			new_velocity.normalise()
			new_velocity = new_velocity*self.max_speed #always move with  max speed

		new_orientation = 90 if self.right_side else -90
		new_position = new_velocity + self.position

		#update state for next round
		self.velocity = new_velocity
		self.position = new_position
		self.orientation = new_orientation

		#pass update to own paddle
		self.paddle.update(new_position.x, new_position.y, new_orientation)
