#!/usr/bin/python

import numpy as np
import cv2
import math


MIN_MATCH_COUNT = 10
MAX_MATCH_COUNT = 20

cv2.namedWindow('img', cv2.WINDOW_NORMAL)
cv2.resizeWindow('img', 1366, 768)
cv2.moveWindow('img', 0, 0)

orb = cv2.ORB_create()
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
cap = cv2.VideoCapture(-1)

image_path = "img2.jpg"
display = cv2.imread(image_path, -1)       # Colour version, for display
background = display.copy()                # Save background image for refilling image
img1 = cv2.imread(image_path, 0)           # BW version, for feature matching
image_height, image_width = img1.shape[:2]
kp1, des1 = orb.detectAndCompute(img1,None)

times = []
while(True):
    start_time = cv2.getTickCount()
    # Capture frame-by-frame
    ret, img2 = cap.read()
    if not ret:
        print "No image from camera"
        continue

    camera_height, camera_width = img2.shape[:2]
    camera_refpoints = [[camera_width/2, camera_height/2], [camera_width/2, 0]]
    kp2, des2 = orb.detectAndCompute(img2, None)

    if des2 is None:
        print "No matches found."
        cv2.imshow('img', display)

    else:
        matches = bf.match(des1,des2)
        matches = sorted(matches, key = lambda x:x.distance)

        good = matches[:MAX_MATCH_COUNT]

        if len(good)<MIN_MATCH_COUNT:
            print "Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT)
            cv2.imshow('img', display)

        else:
            src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
            dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
            M, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC,5.0)
            
            src = np.float32(camera_refpoints).reshape(-1,1,2)
            dst = cv2.perspectiveTransform(src,M)

            middle = (dst[0][0][0], dst[0][0][1])
            top = (dst[1][0][0], dst[1][0][1])
            dx = top[0]-middle[0]
            dy = top[1]-middle[1]
            angle = math.atan2(dy, dx)
            angle_score = abs(angle/math.pi)

            angle = (270 - math.degrees(angle)) % 360
            print angle
            
            radius = int(angle_score*60)

            # Create backup of region covered by circle, to avoid reloading the whole image each frame
            changed_top = max(0, middle[1]-radius-1)
            changed_bottom = min(image_height, middle[1] + radius + 2)
            changed_left = max(0, middle[0]-radius-1)
            changed_right = min(image_width, middle[0] + radius + 2)

            cv2.circle(display, middle, radius, 255, -1)
            cv2.imshow('img', display)
            display[changed_top:changed_bottom, 
                    changed_left:changed_right] = background[changed_top:changed_bottom, 
                                                             changed_left:changed_right]

    # Display the resulting frame
    keypressed = cv2.waitKey(100) & 0xFF
    if keypressed == ord('q'):
        break
    end_time = cv2.getTickCount()
    time_per_loop = (end_time - start_time)/ cv2.getTickFrequency()
    times.append(time_per_loop)
    N = 10
    if len(times) >= N:
        print "{0: .{1}} loops per second.".format(len(times)/sum(times), 2)
        times = []

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
