#!/usr/bin/python

from ball import *
from campointer import CamPointer
import math
import pygame
import sys
import threading

pygame.init()
pygame.mixer.init()
pygame.font.init()
size = width, height = 1000, 500
screen = pygame.display.set_mode(size, pygame.FULLSCREEN)

from catball_bot import catball_bot #need to import this after initializing pygame


class Player:
    def __init__(self, x, y, player_img, cam_number, bg_img):
        self.campointer = CamPointer(bg_img, cam_number)
        self.paddle = Paddle(player_img, x, y)

def infinite_loop(function):
    while True:
        function()

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "2p":
        singleplayer = False
    else:
        singleplayer = True

    bg_img = 'img2.jpg'
    background = Background(bg_img)

    player1 = Player(0, height, "paddle.png", 1, bg_img)
    players = [player1]
    if singleplayer:
        bot = catball_bot(max_speed=12, side='right', width=width, height=height)
    else:
        player2 = Player(width, height, "paddle2.png", 2, bg_img)
        players.append(player2)

    speed = Vector(0, 30)
    ball = Ball("ball.png", width/2, height/4, speed)
    hits = {"left": 0, "right": 0}  # Keep track of ball-wall collisions

    # Draw entire screen once, at the start.
    # Afterward, only changed areas are drawn each frame.
    screen.blit(background.image, background.rect)
    pygame.display.flip()

    meow = pygame.mixer.Sound("ping.wav")
    #meow.set_volume(0.1)
    ping = pygame.mixer.Sound("ping.wav")

    clock = pygame.time.Clock()

    for player in players:
        t = threading.Thread(target=infinite_loop, args=(player.campointer.update_pos,))
        t.daemon = True
        t.start()

    while 1:
        clock.tick(20) # Limit FPS

        ball.move()

        if ball.rect.left < 0:
            ball.velocity.x = abs(ball.velocity.x)
            hits["left"] += 1
            ping.play()
        if ball.rect.right > width:
            ball.velocity.x = -abs(ball.velocity.x)
            hits["right"] += 1
            ping.play()
        if ball.rect.top < 0:
            ball.velocity.y = abs(ball.velocity.y)
        if ball.rect.bottom > height:
            ball.velocity.y = -abs(ball.velocity.y)

        if singleplayer:
            bot.update(ball.rect.center, ball.velocity)
            if ball.rect.colliderect(bot.paddle.rect):
                normalvector = Vector(0, -1)
                normalvector.rotate(bot.paddle.angle)
                if ball.velocity.dot_product(normalvector) < 0:
                        normalvector.normalise()
                        ball.velocity.reflect(normalvector)
                        meow.play()

        for player in players:
            ret = player.campointer.get_pos()
            if ret:
                (x, y), angle = ret
                player.paddle.update(x, y, angle)

            if ball.rect.colliderect(player.paddle.rect):
                normalvector = Vector(0, -1)
                normalvector.rotate(player.paddle.angle)
                if ball.velocity.dot_product(normalvector) < 0:
                    normalvector.normalise()
                    ball.velocity.reflect(normalvector)
                    meow.play()

        # Clear screen of objects that have moved from 
        # previous position.
        screen.blit(background.image, background.rect)

        for player in players:
            screen.blit(player.paddle.image, player.paddle.rect)
        if singleplayer:
            screen.blit(bot.paddle.image, bot.paddle.rect)
        
        #Add the score
        scorebox_x = width/2
        scorebox_y = height-20
        text_color = (64, 64, 64)
        font = pygame.font.Font(None, 50)
        text = font.render("{} - {}".format(hits["right"], hits["left"]),
                           1, text_color)
        textpos = text.get_rect(centerx=scorebox_x, centery=scorebox_y)
        
        #Add the scorebox
        scorebox_color = (255, 255, 255)
        scorebox_size = (text.get_width() + 50, text.get_height() + 10)
        scorebox = pygame.Surface(scorebox_size)
        scorebox.fill(scorebox_color)
        scorebox.set_alpha(200)
        scorebox_pos = scorebox.get_rect(centerx=scorebox_x, centery=scorebox_y)
        
        screen.blit(scorebox, scorebox_pos)
        screen.blit(text, textpos)

        #Add framerate
        font = pygame.font.Font(None, 20)
        text = font.render("FPS : {0:.2f}".format(clock.get_fps()), 
                           1, (255, 115, 15))
        textpos = text.get_rect()
        font_width, font_height = textpos.size
        textpos.center = (width-60, font_height)
        screen.blit(text, textpos)

        screen.blit(ball.image, ball.rect)
        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
